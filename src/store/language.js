import { createSlice } from '@reduxjs/toolkit'

const languageSlice = createSlice({
  name: 'language',
  initialState: {
    value: 0
  },
  reducers: {
    th: state => {
      state.value = 0
    },
    en: state => {
      state.value = 1
    }
  }
})

export const { th, en } = languageSlice.actions
export default languageSlice.reducer
