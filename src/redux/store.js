import { configureStore } from '@reduxjs/toolkit'

import languageReducer from '../store/language'

export const store = configureStore({
  reducer: {
    lang: languageReducer
  }
})
