import {
  Link
} from "react-router-dom";
import { useSelector } from 'react-redux'

import './Landing.css';
import ConstString from '../../configs/ConstString'

import headerImage from '../../assets/Landing/image1.png';
import newsImage from '../../assets/Landing/image2.png';

const Landing = () => {

  const lang = useSelector((state) => state.lang.value)

  return (
    <div className="body">
        <div>
          <Link  to="/" className="img-header">
            <img src={headerImage} />
          </Link>

        </div>
        <div className="news-body">
          <h3>{ConstString.news[lang]}</h3>
          <Link  to="/NewsList" className="PrimaryLink">
            <h3>{ConstString.showAll[lang]}</h3>
          </Link>

        </div>
        <div className="news-body">
          <Link  to="/" className="img-body">
            <img src={newsImage} />
          </Link>
          <Link  to="/" className="img-body">
            <img src={newsImage} />
          </Link>
          <Link  to="/" className="img-body">
            <img src={newsImage} />
          </Link>
          <Link  to="/" className="img-body">
            <img src={newsImage} />
          </Link>
        </div>
    </div>
  );
}

export default Landing;
