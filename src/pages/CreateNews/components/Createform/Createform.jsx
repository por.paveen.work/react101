import { Link } from "react-router-dom";
import { useState } from 'react';
import { useSelector } from 'react-redux'
import ConstString from '../../../../configs/ConstString'
import './Createform.css'

export const Createform = () => {
  const lang = useSelector((state) => state.lang.value)
  const [type, setType] = useState('all')
  const [nameTH, setNameTH] = useState();
  const [nameEN, setNameEN] = useState();
  const [data, setdata] = useState();

  const handleSubmit = (event) => {
    console.log('event', event)
  }

  return (
      <form className="addform" onSubmit={handleSubmit}>
        <div className="inputForm">
          <div>
            <label>
              <p className="text">{ConstString.title[lang]} ({ConstString.strThai[lang]})</p>
              <textarea
                className="input"
                type="text"
                name="nameTH"
                onChange={(e) => setNameTH(e.target.value)} />
            </label>
            <label>
              <p className="text">{ConstString.title[lang]} ({ConstString.strEng[lang]})</p>
              <textarea
                className="input"
                type="text"
                name="nameEN"
                onChange={(e) => setNameEN(e.target.value)} />
            </label>
            <label>
              <div className="groupInput">
                <p className="text">{ConstString.type[lang]}</p>
                <select className="inputOption" value={type} name="types" onChange={(e)=>setType(e.target.value)}>
                  <option value="all">{ConstString.all[lang]}</option>
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select>
              </div>
            </label>
            <label>
              <p className="text">{ConstString.startDate[lang]}</p>
              <input
                className="inputDate"
                type="date"
                name="date"
                onChange={(e) => setdata(e.target.value)} />
            </label>
          </div>

          <label htmlFor="file-upload" className='uploadBtn'>
            +
          </label>
          <input id="file-upload" type="file" name="img" />
        </div>


        <label className="groupButton">
          <input className="cancelBtn" type="button" value={ConstString.cancel[lang]} />
          <input className="submitBtn" type="submit" value={ConstString.save[lang]} />
        </label>
      </form>
  );
}
