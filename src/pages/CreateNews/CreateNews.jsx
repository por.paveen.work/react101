import { Link } from "react-router-dom";

import { Createform } from './components/Createform/Createform'
import { useSelector } from 'react-redux'

import './CreateNews.css';
import ConstString from '../../configs/ConstString'

const CreateNews = () => {
  const lang = useSelector((state) => state.lang.value)

  return (
    <div className="body">
        <div>
          <h2>{ConstString.home[lang]} > {ConstString.news[lang]} > {ConstString.add[lang]}</h2>
          <Createform />
        </div>
    </div>
  );
}

export default CreateNews;
