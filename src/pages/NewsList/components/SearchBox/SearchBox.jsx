import { Link } from "react-router-dom";
import { useState } from 'react';
import { useSelector } from 'react-redux'
import ConstString from '../../../../configs/ConstString'
import './SearchBox.css'

export const SearchBox = () => {
  const lang = useSelector((state) => state.lang.value)
  const [type, setType] = useState('all')
  const [name, setName] = useState();

  const handleSubmit = (event) => {
    console.log('event', event)
  }

  return (
      <form className="form" onSubmit={handleSubmit}>
        <div className="Inputform" >
          <label>
            <div className="groupInput">
              <p className="text">{ConstString.title[lang]}</p>
              <input
                className="input"
                type="text"
                name="name"
                onChange={(e) => setName(e.target.value)} />
            </div>
          </label>
          <label>
            <div className="groupInput">
              <p className="text">{ConstString.type[lang]}</p>
              <select className="input" value={type} name="types" onChange={(e)=>setType(e.target.value)}>
                <option value="all">{ConstString.all[lang]}</option>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
              </select>
            </div>
          </label>
        </div>
        <label className="groupSubmit">
          <p className="text"></p>
          <input className="input" type="submit" value={ConstString.search[lang]} />
        </label>
      </form>
  );
}
