import { Link } from "react-router-dom";
import { useState } from 'react';
import { useSelector } from 'react-redux'
import ConstString from '../../../../configs/ConstString'
import './NewsTable.css'

export const NewsTable = () => {
  const lang = useSelector((state) => state.lang.value)

  return (
    <div>
      <table>
        <tbody>
          <tr>
            <th></th>
            <th>{ConstString.title[lang]}</th>
            <th>{ConstString.type[lang]}</th>
            <th>{ConstString.editDate[lang]}</th>
            <th></th>
          </tr>
          <tr>
            <td>1</td>
            <td>เรื่องที่หนึ่ง ปุก้าๆๆๆๆๆๆๆๆๆ</td>
            <td>{ConstString.news[lang]}</td>
            <td>23/05/2022 13:30 น.</td>
            <td>
              <Link to="/"><p className="PrimaryLink">{ConstString.edit[lang]}</p></Link>
              &nbsp;&nbsp;
              <Link to="/"><p className="PrimaryLink">{ConstString.viewInfo[lang]}</p></Link>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>เรื่องที่หนึ่ง ปุก้าๆๆๆๆๆๆๆๆๆ</td>
            <td>{ConstString.news[lang]}</td>
            <td>23/05/2022 13:30 น.</td>
            <td>
              <Link to="/"><p className="PrimaryLink">{ConstString.edit[lang]}</p></Link>
              &nbsp;&nbsp;
              <Link to="/"><p className="PrimaryLink">{ConstString.viewInfo[lang]}</p></Link>
            </td>
          </tr>
          <tr>
            <td>3</td>
            <td>เรื่องที่หนึ่ง ปุก้าๆๆๆๆๆๆๆๆๆ</td>
            <td>{ConstString.news[lang]}</td>
            <td>23/05/2022 13:30 น.</td>
            <td>
              <Link to="/"><p className="PrimaryLink">{ConstString.edit[lang]}</p></Link>
              &nbsp;&nbsp;
              <Link to="/"><p className="PrimaryLink">{ConstString.viewInfo[lang]}</p></Link>
            </td>
          </tr>
          <tr>
            <td>4</td>
            <td>เรื่องที่หนึ่ง ปุก้าๆๆๆๆๆๆๆๆๆ</td>
            <td>{ConstString.announcement[lang]}</td>
            <td>23/05/2022 13:30 น.</td>
            <td>
              <Link to="/"><p className="PrimaryLink">{ConstString.edit[lang]}</p></Link>
              &nbsp;&nbsp;
              <Link to="/"><p className="PrimaryLink">{ConstString.viewInfo[lang]}</p></Link>
            </td>
          </tr>
          <tr>
            <td>5</td>
            <td>เรื่องที่หนึ่ง ปุก้าๆๆๆๆๆๆๆๆๆ</td>
            <td>{ConstString.notify[lang]}</td>
            <td>23/05/2022 13:30 น.</td>
            <td>
              <Link to="/"><p className="PrimaryLink">{ConstString.edit[lang]}</p></Link>
              &nbsp;&nbsp;
              <Link to="/"><p className="PrimaryLink">{ConstString.viewInfo[lang]}</p></Link>
            </td>
          </tr>
          <tr>
            <td>6</td>
            <td>เรื่องที่หนึ่ง ปุก้าๆๆๆๆๆๆๆๆๆ</td>
            <td>{ConstString.announcement[lang]}</td>
            <td>23/05/2022 13:30 น.</td>
            <td>
              <Link to="/"><p className="PrimaryLink">{ConstString.edit[lang]}</p></Link>
              &nbsp;&nbsp;
              <Link to="/"><p className="PrimaryLink">{ConstString.viewInfo[lang]}</p></Link>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
