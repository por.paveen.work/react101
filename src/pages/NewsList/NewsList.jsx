import { Link } from "react-router-dom";

import { SearchBox } from './components/SearchBox/SearchBox'
import { NewsTable } from './components/NewsTable/NewsTable'
import { useSelector } from 'react-redux'

import './NewsList.css';
import ConstString from '../../configs/ConstString'

const NewsList = () => {
  const lang = useSelector((state) => state.lang.value)

  return (
    <div className="body">
        <div>
          <h2>{ConstString.home[lang]} > {ConstString.news[lang]}</h2>
          <SearchBox />
          <Link to="/CreateNews" className="PrimaryLink"> +{ConstString.addNews[lang]}</Link>
          <NewsTable />
        </div>
    </div>
  );
}

export default NewsList;
