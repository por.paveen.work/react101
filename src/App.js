import React, { Suspense, lazy } from 'react';
import { Routes, Route, Router } from 'react-router-dom';


import Header from './components/Header'
import Footer from './components/Footer'
import './App.css';
const Landing = React.lazy(() => import('./pages/Landing'));
const NewsList = React.lazy(() => import('./pages/NewsList'));
const CreateNews = React.lazy(() => import('./pages/CreateNews'));


const App = () => {
  return (
    <div className="App">
      <Header />
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
            <Route path="/" element={<Landing />}/>
            <Route path="/newsList" element={<NewsList />}/>
            <Route path="/createNews" element={<CreateNews />}/>
        </Routes>
      </Suspense>
      <Footer />
    </div>
  );
}

export default App;
