import logo from '../../assets/logo.png';
import './Header.css';
import {
  Link
} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux'
import {th, en} from '../../store/language'

const Header = () => {
  const lang = useSelector((state) => state.lang.value)
  const dispatch = useDispatch()

  return (
    <div className="header">
        <Link  to="/">
          <img src={logo} className="App-logo" alt="logo" />
        </Link>
        <div className="header">
          {(lang == 0)?<h3 onClick={()=>{dispatch(th())}}>TH</h3>:<p onClick={()=>{dispatch(th())}}>TH</p>}
          <p>|</p>
          {(lang == 1)?<h3 onClick={()=>{dispatch(en())}}>EN</h3>:<p onClick={()=>{dispatch(en())}}>EN</p>}
        </div>
    </div>
  );
}

export default Header;
